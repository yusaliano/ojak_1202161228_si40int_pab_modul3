/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ojak_1202161228_si40int_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/***
 * The adapter class for the RecyclerView, contains the sports data.
 */
class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>  {

    private ArrayList<polaItem> listUser;
    private Context mContext;

    public ListAdapter(ArrayList<polaItem> listUser, Context mContext) {
        this.listUser = listUser;
        this.mContext = mContext;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item, viewGroup, false ));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        polaItem user = listUser.get(position);
        holder.bindTo(user);
    }



    @Override
    public int getItemCount() {
        return listUser.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nama,job;

        private ImageView foto;
        private int avatarCode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.txtNama);
            job = itemView.findViewById(R.id.txtJob);
            foto = itemView.findViewById(R.id.avatar);

            itemView.setOnClickListener(this);

        }

        public void bindTo(polaItem user) {
            nama.setText(user.getNama());
            job.setText(user.getJob());

            avatarCode = user.getAvatar();
            switch (user.getAvatar()){
                case 1:
                    foto.setImageResource(R.drawable.mars);
                    break;
                case 2:
                default:
                    foto.setImageResource(R.drawable.venus);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),DetailActivity.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("job",job.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }
}

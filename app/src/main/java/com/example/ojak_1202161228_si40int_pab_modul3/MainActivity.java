/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ojak_1202161228_si40int_pab_modul3;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {


    Button cancel, submit;
    RecyclerView mRecyclerView;
    Dialog dialog;
    private ArrayList<polaItem> daftarUser;
    private ListAdapter userAdapter;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mRecyclerView = findViewById(R.id.recycler_view);
        int gridColumnCount =getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,gridColumnCount));

        daftarUser = new ArrayList<>();

        if (savedInstanceState != null){
            daftarUser.clear();

            for (int i= 0; i< savedInstanceState.getStringArrayList("nama").size(); i++){

                daftarUser.add(new polaItem(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("job").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));

            }
        }

        userAdapter = new ListAdapter(daftarUser, this);
        mRecyclerView.setAdapter(userAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopup(view);
            }
        });

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT ) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                int dari = viewHolder.getAdapterPosition();
                int ke = target.getAdapterPosition();
                Collections.swap(daftarUser, dari, ke);
                userAdapter.notifyItemMoved(dari,ke);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                daftarUser.remove(viewHolder.getAdapterPosition());
                userAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());

            }
        });
        helper.attachToRecyclerView(mRecyclerView);
    }


    public void ShowPopup(View v) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.popup);
        final TextView mNama,mPekerjaan;
        final Spinner mGender;
        mNama = dialog.findViewById(R.id.edt_nama);
        mPekerjaan = dialog.findViewById(R.id.edt_job);

        Button tambah=dialog.findViewById(R.id.btn_submit);
        Button batal = dialog.findViewById(R.id.btn_cancel);

        mGender = dialog.findViewById(R.id.spinner);

        String[]list={"Male","Female"};

        ArrayAdapter<String> adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftarUser.add(new polaItem(mNama.getText().toString(),mPekerjaan.getText().toString(),mGender.getSelectedItemPosition()+1));
                userAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onSaveInstanceState(Bundle state) {
        ArrayList<String> tmpNama = new ArrayList<>();
        ArrayList<String> tmpJob = new ArrayList<>();
        ArrayList<Integer> tmpGender = new ArrayList<>();

        for (int i=0; i<daftarUser.size(); i++){
            tmpNama.add(daftarUser.get(i).getNama());
            tmpJob.add(daftarUser.get(i).getJob());
            tmpGender.add(daftarUser.get(i).getAvatar());
        }

        state.putStringArrayList("nama", tmpNama);
        state.putStringArrayList("job", tmpJob);
        state.putIntegerArrayList("gender", tmpGender);

        super.onSaveInstanceState(state);
    }



}
